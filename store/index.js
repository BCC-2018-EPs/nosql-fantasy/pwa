import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: {
            game_classes: null,
            locations: null,
            cur_char: null,
            user: null,
            cur_loc: null,
            cur_path: null,
            signup: {
                login: '',
                password: ''
            },
            login: {
                login: '',
                password: ''
            },
        },
    });
}

export default createStore;
